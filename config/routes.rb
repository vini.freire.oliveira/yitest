Rails.application.routes.draw do
  resources :reports, only: [:index, :show, :create]
  resources :comments, except: [:edit]
  resources :events, only: [:index, :show]
end
