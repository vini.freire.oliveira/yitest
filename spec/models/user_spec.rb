require 'rails_helper'

RSpec.describe User, type: :model do

  before(:each) do
    @user = FactoryBot.create(:user)
  end
  
  describe 'Validations' do
    context 'email' do
      it 'should be unique' do
        user_repeated = FactoryBot.create(:user)
        user_repeated.email = @user.email
        expect(user_repeated).to_not be_valid
      end
      it 'invalid' do
        user = FactoryBot.create(:user)
        user.email = "invalid_email.com"
        expect(user).to_not be_valid
      end
    end  
  end 
end
