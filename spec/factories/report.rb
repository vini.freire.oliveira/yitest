FactoryBot.define do
    factory :report do
        user
        comment
    end
end