FactoryBot.define do
    factory :comment do
        text         { FFaker::Lorem.sentence }
        user
        event
    end
end