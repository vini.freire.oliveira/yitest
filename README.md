<h2>Y1Teste Tutorial step by step to run this api</h2>



<h5>System dependencies</h5>

* Docker (https://www.docker.com/)
* Docker-compose



<h5>Step by step to run the project in localhost using docker-compose</h5> 

1. Clone the project from github

   ```bash
   git clone https://gitlab.com/vini.freire.oliveira/yitest.git
   ```

2. Open the project folder

   ```bash
   cd YiTest
   ```

3. Build the containers and install the dependecies

   ```dockerfile
   docker-compose run --rm app bundle install
   ```

4. Now, create database, generate migrations and run the seeds file

   ```ruby
   docker-compose run --rm app bundle exec rails db:create db:migrate db:seed
   ```

5. Start the server with

   ```
   docker-compose up
   ```

   

 <h5>To run Rspec test results</h5>

1. To run the model user spec

   ```dockerfile
   docker-compose run --rm app bundle exec rspec spec/models/user_spec.rb
   ```

2. To run the controller comments  spec

   ```dockerfile
   docker-compose run --rm app bundle exec rspec spec/controllers/comments_controller_spec.rb
   ```



 <h5>Links</h5>

 <h5><a href="https://documenter.getpostman.com/view/110504/Rztpr8RJ">Api end points document</a></h5>

 <h5><a href="https://gitlab.com/vini.freire.oliveira">Git</a></h5>

 <h5><a href="https://www.linkedin.com/in/vinicius-freire-b53507107/">LikedIn</a></h5>



 <h5>Extra docker-compose commands</h5>

1. Start the container in shared folder

   ```
   docker-compose run --rm app bash
   ```

2. Start the ruby container in ruby console

   ```
   docker-compose run --rm app bundle exec rails c
   ```

3. Generate the model using docker-compose

   ```
   docker-compose run --rm app bundle exec rails generate model comment text:text user:references event:references
   ```

4. Generate the controller

   ```
   docker-compose run --rm app bundle exec rails g controller reports index show create update destroy
   ```

   