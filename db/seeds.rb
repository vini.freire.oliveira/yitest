# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


vini = User.create(name: "Vinicius Freire", email: "vini@email.com", password_digest: "123456")
jeni = User.create(name: "Jeniffer", email: "jeni@email.com", password_digest: "123456")


Event.create(name: "Jogo FOR x KANAL", description: "14/02 15:00", lat: 1.156151, lng: 1.754454)
Event.create(name: "Sziget", description: "30/08 168 hrs", lat: 1.156151, lng: 1.754454)
movie = Event.create(name: "Oscar", description: "24/02 20:30", lat: 1.156151, lng: 1.754454)


comment = Comment.create(text: "Bohemian Rhapsody won best movie award", user_id: vini.id, event_id: movie.id)

Report.create(user_id: jeni.id, comment_id: comment.id)