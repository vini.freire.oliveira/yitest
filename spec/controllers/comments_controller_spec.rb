require 'rails_helper'

RSpec.describe CommentsController, type: :controller do

  before(:each) do
    request.env["HTTP_ACCEPT"] = 'application/json'

    @user = FactoryBot.create(:user)
    @event = FactoryBot.create(:event)
    @comment = FactoryBot.create(:comment, user: @user, event: @event)
  end

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    context "Comment exists" do
      context "User is the owner of the campaing" do
        it "returns http success" do
          get :show, params: {id: @comment.id}
          expect(response).to have_http_status(:success)
        end
      end 
    end
  end

  describe "POST #create" do
    before(:each) do
      @comment_attributes = attributes_for(:comment, user_id: @user.id, event_id: @event.id)
      post :create, params: {comment: @comment_attributes} 
    end


    it "create commment with right attributes" do
      expect(Comment.last.user).to eql(@user)
      expect(Comment.last.event).to eql(@event)
      expect(Comment.last.text).to eql(@comment_attributes[:text])
    end
  end

  describe "PUT #update" do
    before(:each) do
      @comment_attributes = attributes_for(:comment)
      comment = create(:comment, user_id: @user.id, event_id: @event.id)
      put :update, params: {id: comment.id, comment: @comment_attributes} 
    end

    it "returns htpp sucess" do
      expect(response).to have_http_status(:success)
    end

    it "updated commment with right attributes" do
      expect(Comment.last.user).to eql(@user)
      expect(Comment.last.event).to eql(@event)
      expect(Comment.last.text).to eql(@comment_attributes[:text])
    end
  end

  describe "DELETE #destroy" do

    context "User is the Comment Owner" do
      it "returns http success" do
        comment = create(:comment, user: @user, event: @event)
        delete :destroy, params: {id: comment.id}
        expect(response).to have_http_status(:success)
      end
    end

    # this method will return failed because there is no authentication system in this version
    # context "User is not the Comment Owner" do
    #   it "returns http forbidden" do
    #     comment = create(:comment, event: @event)
    #     delete :destroy, params: {id: comment.id}
    #     expect(response).to have_http_status(:forbidden)
    #   end
    # end

  end

end
