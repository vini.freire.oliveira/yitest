class ReportsController < ApplicationController
  before_action :set_report, only: [:show]

  def index
    @reports = Report.all
    render json: @reports, include: [:user, :comment]
  end

  def show
    render json: @report, include: [:user, :comment]
  end

  def create
    @report = Report.new(report_params)
    if @report.save
      render json: @report, include: [:user, :comment]
    else
      render json: @report.errors.full_messages, status: :unprocessable_entity
    end
  end

  private

  def set_report
    @report = Report.find(params[:id])
  end


  def report_params
    params.require(:report).permit(:user_id, :comment_id)
  end

end
