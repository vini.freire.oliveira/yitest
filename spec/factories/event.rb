FactoryBot.define do
    factory :event do
        name         { FFaker::Lorem.word }
        description  { FFaker::Lorem.sentence }
        lat          { FFaker::Geolocation.lat }
        lng          { FFaker::Geolocation.lng }
    end
end