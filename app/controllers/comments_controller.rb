class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :update, :destroy]
  
  def index
    @comments = Comment.all
    render json: @comments, include: [:user, :event]
  end

  def show
    render json: @comment, include: [:user, :event]
  end

  def create
    @comment = Comment.new(comment_params)
    if @comment.save
      render json: @comment, include: [:user, :event]
    else
      render json: @comment.errors.full_messages, status: :unprocessable_entity
    end
  end

  def update
    if @comment.update(comment_params)
      render json: @comment, include: [:user, :event]
    else
      render json: @comment.errors.full_messages, status: :unprocessable_entity
    end
  end

  def destroy
    if @comment.destroy
      render json: :destroyed
    else
      render json: @task.errors.full_messages, status: :unprocessable_entity
    end
  end

  private

  def set_comment
    @comment = Comment.find(params[:id])
  end


  def comment_params
    params.require(:comment).permit(:text, :user_id, :event_id)
  end
end
